--[
-- This file is part of Unoriented ROC IPElets.
--
-- Copyright 2018 Carlos Alegría Galicia
--
-- Unoriented ROC IPElets is free software: you can redistribute it
-- and/or modify it under the terms of the GNU General Public License
-- as published by the Free Software Foundation, either version 3 of
-- the License, or any later version.
--
-- Unoriented ROC IPElets is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with Unoriented ROC IPElets. If not, see
-- <http://www.gnu.org/licenses/>.
--]

--------------------------------------------------------------------------------
-- Unoriented Rectilinear Convex Hull Ipelet
--
-- Carlos Alegría Galicia
--------------------------------------------------------------------------------

-- Ipe gui related variables ---------------------------------------------------

label = "Rectilinear Convex Hull"

about = [[
    Select a set of points to calculate the RCH using the default axis
    orientation. Use axis snapping to speficy a different orientation.
]]

-- global variables ------------------------------------------------------------

-- vector object used to generate RCH's vertices
--
V = ipe.Vector
L = ipe.LineThrough


-- methods ---------------------------------------------------------------------

-- Obtains the RCH of a set of points, using the default coordinate system or a
-- rotated coordinate system specified by the user
--
function rectilinear_convex_hull_ipelet(model, id)
    
    -- 
    --  obtaining selected points
    --


    local p = model:page()
    
    -- checking that current selection is not empty
    --
    if not p:hasSelection()
    then
        model:warning("No selection found")
        return
    end
   
    -- extracting selected reference objects
    --
    local points = {}
    for i, obj, sel, layer in p:objects()
    do
        if sel
        then
            if obj:type() == "reference" and obj:symbol():sub(1,5) == "mark/"
            then
                -- insert the vector asociated with each reference
                --
                table.insert(points, obj:matrix() * obj:position())
                
            elseif obj:type() == "group"
            then
                for index, mark in ipairs(obj:elements())
                do
                    if mark:type() == "reference" and mark:symbol():sub(1,5) == "mark/"
                    then
                        table.insert(points, obj:matrix() * mark:position())
                    end
                end
            end
        end
    end
    
    -- to create a RCH with need at least two points
    --
    if #points < 2
    then
        model:warning("At least two marks need to be selected")
        return
    end    
        
    -- calculating the RCH
    --
    if id == 1
    then
        --
        --  single orientation
        --
        
        -- for an specific orientation, we rotate the points clockwise, which is
        -- equivalent to a counter-clockwise rotation of the coordinate system
        --
        if (model.snap.with_axes)
        then
            local matrix = ipe.Rotation(-model.snap.orientation)
            for i = 1, #points
            do
                points[i] = matrix * points[i]
            end
        end

        -- calculating the rch
        --    
        local rch = rectilinear_convex_hull(points)

        -- expressing the RHC in the standard coordinate system, if needed
        --
        local degrees
        if (model.snap.with_axes)
        then
            degrees = math.deg(model.snap.orientation)
            local matrix = ipe.Rotation(model.snap.orientation)
        
            for i = 1, #rch
            do
                for j = 1, #rch[i]
                do
                    rch[i][j][1] = matrix * rch[i][j][1]
                    rch[i][j][2] = matrix * rch[i][j][2]
                end
            end
        else
            degrees = 0
        end

        -- creating the RCH's path
        --
        local path = ipe.Path(model.attributes, rch)
        model:creation(
            string.format('Rectilinear Convex Hull at %.3f degrees', degrees),
            path)
    else
        --
        --  all orientations
        --
        
        
        -- obtaining angle step
        --
        local str = model:getString("Enter angle step in degrees")
        local step
        
        if str and not str:match("^%s*$") then step = tonumber(str) end
        if not step or step <= 0 or step >= 90
        then
            model:warning("Angle step should be a number between 0 and 90 (exclusive)")
            return
        end
        
        -- 
        --
        local rotated_points = {}
        local matrix
        local rch
        local path
        
        -- rotation angle in radians
        --
        --for angle = 0, math.pi / 2 - ((90 % step == 0) and math.rad(step) or 0),
        --    math.rad(step)
        for angle = 0, math.pi / 2, math.rad(step)
        do
            rotated_points = {}
            
            -- rotating points
            --
            matrix = ipe.Rotation(-angle)
            for i = 1, #points
            do
                table.insert(rotated_points, matrix * points[i])
            end
            
            -- calculating the rch
            --    
            rch = rectilinear_convex_hull(rotated_points)

            -- expressing the RHC in the standard coordinate system
            --
            matrix = ipe.Rotation(angle)
        
            for i = 1, #rch
            do
                for j = 1, #rch[i]
                do
                    rch[i][j][1] = matrix * rch[i][j][1]
                    rch[i][j][2] = matrix * rch[i][j][2]
                end
            end
            
            -- creating and registering the RCH's path
            --
            path = ipe.Path(model.attributes, rch)
            model:creation('', path)
        end
    end
end

--
--
function rectilinear_convex_hull(points)

    -- rch can be reduced to the set maxima calculation in each cuadrant
    --
    quad_1 = set_maxima(points,
                function(p,q) return p.x > q.x end,
                function(p,q) return p.x == q.x end,
                function(p,q) return p.y >= q.y end)
    quad_2 = set_maxima(points,
                function(p,q) return p.y > q.y end,
                function(p,q) return p.y == q.y end,
                function(p,q) return p.x <= q.x end)
    quad_3 = set_maxima(points,
                function(p,q) return p.x < q.x end,
                function(p,q) return p.x == q.x end,
                function(p,q) return p.y <= q.y end)
    quad_4 = set_maxima(points,
                function(p,q) return p.y < q.y end,
                function(p,q) return p.y == q.ys end,
                function(p,q) return p.x >= q.x end)

    --
    -- creating the Ipe Path
    --
    
    local rch_shape = {}
    local stair = {}
    
    -- staircase 1
    --
    if #quad_1 ~= 1
    then
        table.insert(rch_shape,
            create_staircase(quad_1, function(p, q) return V(p.x, q.y) end))
    end
    
    -- staircase 4
    --
    if #quad_4 ~= 1
    then
        stair = create_staircase(quad_4, function(p, q) return V(q.x, p.y) end)
        table.insert(rch_shape, stair)
    end
    
    -- staircase 3
    --
    if #quad_3 ~= 1
    then
        stair = create_staircase(quad_3, function(p, q) return V(p.x, q.y) end)
        table.insert(rch_shape, stair)
    end
    
    -- staircase 2
    --
    if #quad_2 ~= 1
    then
        stair = create_staircase(quad_2, function(p, q) return V(q.x, p.y) end)
        table.insert(rch_shape, stair)
    end
    
    return rch_shape
end

-- obtains the maxima of a 2d Vector set
--
function set_maxima(points, x_gt_comp, x_eq_comp, y_gteq_comp)
    local maximal
    local maxima = {}
       
    -- sort points by decreasing x coordinate
    -- 
    table.sort(points, x_gt_comp)
    
    -- obtain the maximal points. as they are already ordered in by x
    -- coordinate, we know that each point is a x-maximal. we only need to
    -- check y coordinate dominance
    --
    
    -- insert element at the beginning, so the maxima set will
    -- be sorted by increasing x coordinate
    --
    for _,p in ipairs(points)
    do
        if not maximal
        then
            maximal = p
            table.insert(maxima, 1, p)

        elseif y_gteq_comp(p, maximal)
        then
            if x_eq_comp(p, maximal)
            then
                table.remove(maxima, 1)
            end

            maximal = p
            table.insert(maxima, 1, p)
        end
    end
    
    return maxima
end

--
--
function create_staircase(quad, middle_point)

    local stair = { type="curve", closed=false }
    local middle
    local line
    
    for i = 1, #quad - 1
    do
        middle = middle_point(quad[i], quad[i + 1])
        line = L(quad[i], quad[i + 1])
        
        if line:distance(middle) == 0
        then
            table.insert(stair, { type="segment", quad[i], quad[i + 1] })
        else
            table.insert(stair, { type="segment", quad[i], middle })
            table.insert(stair, { type="segment", middle, quad[i + 1] })
        end
    end

    return stair
end

-- Ipelet function specification -----------------------------------------------


methods = {
    { label="Oriented",          run = rectilinear_convex_hull_ipelet },
    { label="Unoriented",        run = rectilinear_convex_hull_ipelet }
}


--------------------------------------------------------------------------------
