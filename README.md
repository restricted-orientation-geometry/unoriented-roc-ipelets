Unoriented ROC IPElets
======================

A set of IPElets to compute the Restricted-orientation Convex Hull of
a set of points in the plane [1] for different orientations of the
coordinate axis.

References
----------

[1] https://en.wikipedia.org/wiki/Orthogonal_convex_hull